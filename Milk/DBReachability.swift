//
//  DBReachability.swift
//  Milk
//
//  Created by Jon McLean on 18/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit
import SwiftyJSON

class DBReachability: NSObject {
    
    var timer = NSTimer()
    
    func startTimer() {
        print("start db timer")
        timer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: Selector("isDBAvailable"), userInfo: nil, repeats: true)
    }
    
    func isDBAvailable() { // returns notifier... WARNING: DOES NOT WORK
        print("is db available")
        let hostName = globalURL + "/api/debug/"
        
        let network = Networking()
        network.sendURLRequest(hostName) { (data, response, error) -> Void in
            
            if(error != nil) {
                let handler = ErrorHandler()
                handler.printError(error!, className: "Reachability")
            }
            
            let json = JSON(data: data!)
            let jd = json.dictionaryObject
            print(json)
            let db_speed = jd!["db_speed"] as! Int
            
            if(db_speed != -1) {
            }else {
                
                let alert = UIAlertController(title: "There was a problem", message: "Sadly there is a problem with our servers, sorry for any inconvenience", preferredStyle: .Alert)
                
                let okAction = UIAlertAction(title: "Ok", style: .Default) { (action) -> Void in
                    
                    let signupController = SignupViewController()
                    
                    UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(signupController, animated: true, completion: nil)
                    
                }
                
                alert.addAction(okAction)
                
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
                
            }
        }
    }
    
}
