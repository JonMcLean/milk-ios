//
//  ErrorHandler.swift
//  Milk
//
//  Created by Jon McLean on 9/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit

class ErrorHandler: NSObject {
    
    func printError(error: NSError, className: String) {
        print("[\(className)] has thrown an error with the code: \(error.code)")
        print("[\(className)] The above error had the following description \(error.localizedDescription)")
    }
    
}
