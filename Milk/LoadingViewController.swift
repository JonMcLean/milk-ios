//
//  LoadingViewController.swift
//  Milk
//
//  Created by Jon McLean on 7/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit
import CoreData

var globalURL = "http://default-environment.rk25jpe2mr.us-west-2.elasticbeanstalk.com/"

class LoadingViewController: UIViewController {
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        /*let delegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context = delegate.managedObjectContext
        
        let fetch = NSFetchRequest(entityName: "ShoppingList")
        
        var stRet: [AnyObject] = []
        
        do {
            stRet = try context.executeFetchRequest(fetch)
        }catch {
            print(error)
        }
        
        for i in stRet {
            context.deleteObject(i as! NSManagedObject)
        }
        
        do {
            try context.save()
        }catch {
            print(error)
        }*/
        
        var db = DBReachability()
        db.startTimer()
        
        // check if logged in.
        let session = SessionStorage()
        session.clear()
        
        if(session.exists()) {
            
            if(session.isLoggedIn()) {
                if(session.isValid()) {
                    
                    print("logged in")
                    
                    self.performSegueWithIdentifier("Splash_Main", sender: self)
                    
                }else {
                    self.performSegueWithIdentifier("Splash_Login", sender: self)
                }
            }else {
                self.performSegueWithIdentifier("Splash_Signup", sender: self)
            }
        }else {
            self.performSegueWithIdentifier("Splash_Signup", sender: self)
        }
        
    }
    
}
