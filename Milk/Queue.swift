//
//  Queue.swift
//  Milk
//
//  Created by Jon McLean on 9/05/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import Foundation

struct Queue<Element> {
    
    var queue = [Element]();
    
    mutating func push(object: Element) {
        queue.append(object)
    }
    
    mutating func pop() {
        queue.removeFirst();
    }
    
    func first() -> Element{
        return queue.first!;
    }
    
    func last() -> Element {
        return queue.last!;
    }
    
    func count() -> Int{
        return queue.count;
    }
    
    func empty() -> Bool {
        return queue.isEmpty;
    }
}
