//
//  TagColors.swift
//  Milk
//
//  Created by Jon McLean on 11/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit

var color0 = UIColor(red: 51.0/255, green: 102.0/255, blue: 204.0/255, alpha: 1)
var color1 = UIColor(red: 210.0/255, green: 79.0/255, blue: 87.0/255, alpha: 1)
var color2 = UIColor(red: 83.0/255, green: 37.0/255, blue: 210.0/255, alpha: 1)
var color3 = UIColor(red: 64.0/255, green: 210.0/255, blue: 128.0/255, alpha: 1)
var color4 = UIColor(red: 228.0/255, green: 209.0/255, blue: 31.0/255, alpha: 1)