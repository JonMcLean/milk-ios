//
//  TopShoppingListCell.swift
//  Milk
//
//  Created by Jon McLean on 9/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit

class TopShoppingListCell: UITableViewCell {
    
    @IBOutlet var colorView: UIView!
    @IBOutlet var name: UILabel!
    
    func fillCell(name: String, color: UIColor) {
        self.name.text = name
        self.colorView.backgroundColor = color
    }
    
}
