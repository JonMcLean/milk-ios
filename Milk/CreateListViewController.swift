//
//  CreateListViewController.swift
//  Milk
//
//  Created by Jon McLean on 9/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit
import QuartzCore

class CreateListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var nameField: UITextField!
    @IBOutlet var usersTable: UITableView!
    
    var arrayOfUsers: Array<String> = []
    var nameThing: String = ""
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .Plain, target: self, action: Selector("nextStep"))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .Plain, target: self, action: Selector("cancelAction"))
        
        var tap = UITapGestureRecognizer(target: self, action: Selector("dismissKeyboard"))
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.usersTable.delegate = self
        self.usersTable.dataSource = self
        
        self.nameField.text = self.nameThing
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UserCell", forIndexPath: indexPath)
        
        cell.textLabel!.text = arrayOfUsers[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfUsers.count
    }
    
    func nextStep() {
        //cm.add(self.nameField.text!, color: nil, users: self.arrayOfUsers)
        self.nameThing = self.nameField.text!
        self.performSegueWithIdentifier("Create_Create2", sender: self)
    }
    
    func cancelAction() {
        //cm.setStored(false)
        //cm.clear()
        self.performSegueWithIdentifier("Create_Main", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let segueId = segue.identifier
        if(segueId == "Create_Create2") {
            let destVC = segue.destinationViewController as! CreateList2ViewController
            destVC.nameThing = self.nameThing
            destVC.arrayOfUsers = self.arrayOfUsers
        }
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
}