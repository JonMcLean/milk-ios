//
//  SignupViewController.swift
//  Milk
//
//  Created by Jon McLean on 9/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit
import SwiftyJSON

class SignupViewController: UIViewController {
    
    @IBOutlet var firstNameField: UITextField!
    @IBOutlet var lastNameField: UITextField!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var outcomeLabel: UILabel!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.outcomeLabel.text = ""
        self.passwordField.secureTextEntry = true
        
        var tap = UITapGestureRecognizer(target: self, action: Selector("dismissKeyboard"))
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func signupButton(sender: UIButton) {
        
        let network = Networking()
        network.sendPOSTRequest((globalURL + "/api/auth/signup"), parameters: ["first_name":firstNameField.text!, "last_name":lastNameField.text!, "username":usernameField.text!, "password":passwordField.text!]) { (data, response, error) -> Void in
            
            print("Running signup")
            
            if(error != nil) {
                let handler = ErrorHandler()
                handler.printError(error!, className: "SignupViewController")
            }
            
            print("Hello")
            
            let code = (response as! NSHTTPURLResponse).statusCode
            
            if(code == 200) {
                
                print("All is OK")
                
                self.outcomeLabel.textColor = UIColor.greenColor()
                self.outcomeLabel.text = "Success!"
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    let json = JSON(data: data!)
                    let cDic = json.dictionaryObject
                    
                    print(cDic)
                    
                    let storage = SessionStorage()
                    
                    storage.storeSession(cDic!["session"] as! String, expire: cDic!["expire"] as! Int, username: cDic!["username"] as! String, id: cDic!["user_id"] as! Int, firstName: cDic!["first_name"] as! String, lastName: cDic!["last_name"] as! String, icon: cDic!["icon"] as! String)
                    storage.setLoggedIn(true)
                    
                    self.performSegueWithIdentifier("Signup_Main", sender: self) // move to main view.
                    
                })
                
                
            }else {
                
                self.outcomeLabel.textColor = UIColor.redColor()
                self.outcomeLabel.text = "An error occured while signing up. Please try again later"
                
                print(code)
            }
            
            
            
        }
        
    }
    
    @IBAction func moveToLoginButton(sender: UIButton) {
        self.performSegueWithIdentifier("Signup_Login", sender: self)
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
}
