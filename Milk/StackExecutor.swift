//
//  StackExecutor.swift
//  Milk
//
//  Created by Jon McLean on 11/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class StackExecutor: NSObject {
    
    static var stackTimer = NSTimer()
    static var finish: Array<() -> Void> = []
    
    class func startTimer() {
        self.stackTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: Selector("runStackElement"), userInfo: nil, repeats: true)
    }
    
    class func runStackElement() {
        
        print("running timer")
        
        let delegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context = delegate.managedObjectContext
        
        let fetch = NSFetchRequest(entityName: "Stack")
        
        var ret: [AnyObject] = []
        
        do {
            try ret = context.executeFetchRequest(fetch)
        }catch {
            print(error)
        }
        
        for i in ret {
            
            let api = i.valueForKey("api") as! String
            let method = i.valueForKey("method") as! String
            let params = i.valueForKey("params") as! Dictionary<String, AnyObject>
            let id: NSManagedObjectID = i.valueForKey("local_id") as! NSManagedObjectID
            
            let network = Networking()
            
            if(method == "POST") {
                
                network.sendPOSTRequest(globalURL + api, parameters: params, completionHandler: { (data, response, error) -> Void in
                    
                    if(data != nil) {
                        let jsonReturn = JSON(data: data!)
                        let jd = jsonReturn.dictionaryObject
                        
                        if(api == "/api/post") {
                            
                            do {
                                if let obj: NSManagedObject? = try context.existingObjectWithID(id) {
                                    
                                    if(obj != nil) {
                                        
                                        obj!.setValue(jd!["id"] as! Int, forKey: "server_id")
                                        
                                        do {
                                            try context.save()
                                        }catch {
                                            print(error)
                                        }
                                        
                                    }else {
                                        print("Can't find the object")
                                    }
                                    
                                }
                            }catch {
                                print(error)
                            }
                            
                        }else if(api == "/api/list") {
                            
                            do {
                                if let obj: NSManagedObject? = try context.existingObjectWithID(id) {
                                    
                                    if(obj != nil) {
                                        
                                        obj!.setValue(jd!["id"] as! Int, forKey: "server_id")
                                        
                                        do {
                                            try context.save()
                                        }catch {
                                            print(error)
                                        }
                                        
                                    }else {
                                        print("Can't find the object")
                                    }
                                    
                                }
                            }catch {
                                print(error)
                            }
                            
                        }else {
                            
                        }
                        
                    }else {
                        
                    }
                    
                    
                })
                
            }else if(method == "GET") {
                
                network.sendGETRequest(globalURL + api, parameters: params, completionHandler: { (data, response, error) -> Void in
                    
                })
                
            }else if(method == "PUT") {
                
            }else if(method == "DELETE") {
                
            }
            
            context.deleteObject(i as! NSManagedObject)
            
        }
        
        for(var i = 0; i < finish.count; i++){
            finish.removeAtIndex(0)()
        }
        
    }
    
    class func addEmpty(completion: (() -> Void)!) {
        finish.append(completion)
    }
    
}
