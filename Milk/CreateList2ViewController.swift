//
//  CreateList2ViewController.swift
//  Milk
//
//  Created by Jon McLean on 11/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class CreateList2ViewController: UIViewController {
    
    var selectedColor = UIColor(red: 51, green: 102.0, blue: 204, alpha: 1)
    var colorId: Int = 0
    
    var nameThing: String = ""
    var arrayOfUsers: Array<String> = []
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .Done, target: self, action: Selector("complete"))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .Plain, target: self, action: Selector("backAction"))
    }
    
    @IBAction func redAction(sender: UIButton) {
        self.selectedColor = color1
        self.colorId = 1
    }
    
    @IBAction func blueAction(sender: UIButton) {
        self.selectedColor = color0
        self.colorId = 0
    }
    
    @IBAction func purpleAction(sender: UIButton) {
        self.selectedColor = color2
        self.colorId = 2
    }
    
    @IBAction func greenAction(sender: UIButton) {
        self.selectedColor = color3
        self.colorId = 3
    }
    
    @IBAction func yellowAction(sender: UIButton) {
        self.selectedColor = color4
        self.colorId = 4
    }
    
    func complete() {
        let delegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context = delegate.managedObjectContext
        
        let entity = NSEntityDescription.insertNewObjectForEntityForName("ShoppingList", inManagedObjectContext: context)
        entity.setValue(self.colorId, forKey: "color")
        entity.setValue(nameThing, forKey: "name")
        entity.setValue(0, forKey: "server_id")
        
        do {
            try context.save()
        }catch {
            //let handler = ErrorHandler()
            print(error)
        }
        
        let arrayFetch = NSFetchRequest(entityName: "ShoppingList")
        
        var ret: [AnyObject] = []
        
        do {
            ret = try context.executeFetchRequest(arrayFetch)
        }catch {
            print(error)
        }
        
        let lObj = ret.last
        let id = lObj!.objectID
        
        let stack = NSEntityDescription.insertNewObjectForEntityForName("Stack", inManagedObjectContext: context)
        
        let storage = SessionStorage()
        let dic = storage.loadSession()
        let sessionCode = dic["sessionCode"] as! String
        
        stack.setValue("/api/list/", forKey: "api")
        stack.setValue("POST", forKey: "method")
        
        let json = JSON(arrayOfUsers)
        let str = json.rawString()
        
        stack.setValue(["name": String(nameThing), "color": String(self.colorId), "session": String(sessionCode), "users": String(str)], forKey: "params")
        stack.setValue(id, forKey: "local_id")
        
        do {
            try context.save()
        }catch {
            print(error)
        }
        
        print("Complete")
        self.performSegueWithIdentifier("Create2_Main", sender: self)
    }
    
    func backAction() {
        self.performSegueWithIdentifier("Create2_Create", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let segueId = segue.identifier
        if(segueId == "Create2_Create") {
            let destVC = segue.destinationViewController as! CreateListViewController
            destVC.nameThing = self.nameThing
            destVC.arrayOfUsers = self.arrayOfUsers
        }
    }
    
    
    
}

extension UIView {
    func makeCircular() {
        let cntr:CGPoint = self.center
        self.layer.cornerRadius = min(self.frame.size.height, self.frame.size.width) / 2.0
        self.center = cntr
        self.clipsToBounds = true
    }
}
