//
//  SessionStorage.swift
//  Milk
//
//  Created by Jon McLean on 7/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit

class SessionStorage: NSObject {
    
    let ud = NSUserDefaults.standardUserDefaults()
    
    func storeSession(sessionCode: String, expire: Int, username: String, id: Int, firstName: String, lastName: String, icon: String) {
        
        ud.setBool(true, forKey: "l_LoggedIn")
        ud.setBool(true, forKey: "l_isValid")
        ud.setValue(sessionCode, forKey: "l_session")
        ud.setValue(expire, forKey: "l_expire")
        ud.setValue(username, forKey: "l_username")
        ud.setValue(id, forKey: "l_id")
        ud.setValue(firstName, forKey: "l_firstName")
        ud.setValue(lastName, forKey: "l_lastName")
        ud.setValue(icon, forKey: "l_icon")
        
    }
    
    func loadSession() -> Dictionary<String, AnyObject> {
        let dictionaryReturn: Dictionary<String, AnyObject> = ["sessionCode":ud.valueForKey("l_session")!, "expire":ud.valueForKey("l_expire")!, "username":ud.valueForKey("l_username")!, "id":ud.valueForKey("l_id")!, "firstName": ud.valueForKey("l_firstName")!, "lastName": ud.valueForKey("l_lastName")!, "iconURL": ud.valueForKey("l_icon")!]
        return dictionaryReturn
    }
    
    func setLoggedIn(loggedIn: Bool) {
        ud.setBool(loggedIn, forKey: "l_LoggedIn")
    }
    
    func isLoggedIn() -> Bool {
        return ud.boolForKey("l_LoggedIn")
    }
    
    func invalidate() {
        ud.setBool(false, forKey: "l_isValid")
    }
    
    func isValid() -> Bool {
        return ud.boolForKey("l_isValid")
    }
    
    func clear() {
        self.setLoggedIn(false)
        ud.removeObjectForKey("l_isValid")
        ud.removeObjectForKey("l_session")
        ud.removeObjectForKey("l_expire")
        ud.removeObjectForKey("l_username")
        ud.removeObjectForKey("l_id")
        ud.removeObjectForKey("l_firstName")
        ud.removeObjectForKey("l_lastName")
        ud.removeObjectForKey("l_icon")
    }
    
    func exists() -> Bool{
        if(ud.valueForKey("l_session") != nil) {
            return true
        }
        
        return false
    }
    
}
