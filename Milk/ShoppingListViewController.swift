//
//  ShoppingListViewController.swift
//  Milk
//
//  Created by Jon McLean on 9/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData

class ShoppingListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    
    var json: JSON = []
    var array: [AnyObject] = []
    
    var offline = false
    var index = 0
    var managed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        print("hello world")
        
        if(Reachability.isConnectedToNetwork()) {
            
            let delegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
            let context = delegate.managedObjectContext
            
            StackExecutor.addEmpty({
                print("running thing")
                let fetch = NSFetchRequest(entityName: "ShoppingList")
                
                var ret: [AnyObject] = []
                
                do {
                    ret = try context.executeFetchRequest(fetch)
                }catch {
                    print(error)
                }
                
                self.offline = false
                
                var arrayOfObjs: Array<Dictionary<String, AnyObject>> = []
                
                for i in ret {
                    
                    let obj = i as! NSManagedObject
                    
                    let name = obj.valueForKey("name") as! String
                    let color = (obj.valueForKey("color") as! Int)
                    let server_id = (obj.valueForKey("server_id") as! Int)
                    
                    var dic: [String: AnyObject] = ["color":color, "name":name, "id":server_id]
                    
                    arrayOfObjs.append(dic)
                }
                
                /*let json = JSON(arrayOfObjs)
                //print(json)
                
                var jsonString = json.rawString()*/
                
                let serial = CJSONSerializer()
                
                var jsonString = ""
                
                do {
                    let data = try serial.serializeObject(arrayOfObjs)
                    let ns = NSString(data: data, encoding: NSUTF8StringEncoding)
                    jsonString = String(ns)
                }catch {
                    print(error)
                }
                
                
                jsonString = jsonString.stringByReplacingOccurrencesOfString("\n", withString: "")
                
                print("json: \(jsonString)")
                
                let hash = jsonString.sha256
                print(hash)
                
                let network = Networking()
                
                let storage = SessionStorage()
                let cDic = storage.loadSession()
                
                let session = cDic["sessionCode"] as! String
                //print(session)
                
                network.sendURLRequest(globalURL + "/api/list/" + session + ".sha256", completionHandler: { (data, response, error) -> Void in
                    let nstring = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("Beginning Networking")
                    if(String(nstring) == hash) {
                        
                        print("Hello World")
                        
                        self.managed = true
                        
                        let fetch = NSFetchRequest(entityName: "ShoppingList")
                        
                        do {
                            self.array = try context.executeFetchRequest(fetch)
                        }catch {
                            print(error)
                        }
                        
                    }else {
                        
                        self.managed = false
                        
                        network.sendURLRequest(globalURL + "/api/list/" + session, completionHandler: { (data, response, error) -> Void in
                            let json = JSON(data: data!)
                            
                            let jsonArray = json.arrayObject
                            self.array = jsonArray!
                            
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                
                                let delegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
                                let context = delegate.managedObjectContext
                                
                                let fetch = NSFetchRequest(entityName: "ShoppingList")
                                
                                var stRet: [AnyObject] = []
                                
                                do {
                                    stRet = try context.executeFetchRequest(fetch)
                                }catch {
                                    print(error)
                                }
                                
                                for i in stRet {
                                    context.deleteObject(i as! NSManagedObject)
                                }
                                
                                do {
                                    try context.save()
                                }catch {
                                    print(error)
                                }
                                
                                self.tableView.reloadData()
                            })
                        })
                    }
                })
                
                
            });
            
        }else {
            self.offline = true
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TopLevShopListCell", forIndexPath: indexPath) as! TopShoppingListCell
        
        var name = ""
        var color = 0
        print(array)
        
        if(!managed) {
            print("Not managed")
            let dic = self.array[indexPath.row] as! Dictionary<String, AnyObject>
            name = dic["name"] as! String
            color = (dic["color"] as! NSString).integerValue
            
            let id = dic["id"] as! Int
            
            let delegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
            let context = delegate.managedObjectContext
            
            let entity = NSEntityDescription.insertNewObjectForEntityForName("ShoppingList", inManagedObjectContext: context)
            entity.setValue(name, forKey: "name")
            entity.setValue(color, forKey: "color")
            entity.setValue(id, forKey: "server_id")
            
            do {
                try context.save()
            }catch {
                print(error)
            }
        }else {
            let object: NSManagedObject = self.array[indexPath.row] as! NSManagedObject
            name = object.valueForKey("name") as! String
            color = object.valueForKey("color") as! Int
            
            print(name)
        }
        
        if(!offline) {
            
            
            
        }else {
            let object: NSManagedObject = self.array[indexPath.row] as! NSManagedObject
            name = object.valueForKey("name") as! String
            color = object.valueForKey("color") as! Int
            
            print(name)
        }
        
        var actualColor: UIColor = UIColor.blueColor()
        
        if(color == 0) {
            actualColor = color0
        }else if(color == 1) {
            actualColor = color1
        }else if(color == 2) {
            actualColor = color2
        }else if(color == 3) {
            actualColor = color3
        }else if(color == 4) {
            actualColor = color4
        }else {
            actualColor = color0
        }
        
        cell.fillCell(name, color: actualColor)
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("Count: \(array.count)")
        return array.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        // open detail view through segue data
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.index = indexPath.row
        self.performSegueWithIdentifier("Main_Detail", sender: self)
        
    }
    
    @IBAction func openCreateMenu(sender: UIBarButtonItem) {
        self.performSegueWithIdentifier("Main_Create", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let segueId = segue.identifier
        
        if(segueId == "Main_Detail") {
            
            let destVC = segue.destinationViewController as! ShoppingListContentViewController
            
            let delegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
            let context = delegate.managedObjectContext
            
            let fetch = NSFetchRequest(entityName: "ShoppingList")
            
            var ret: [AnyObject] = []
            
            do {
               ret = try context.executeFetchRequest(fetch)
            }catch {
                print(error)
            }
            
            let obj = ret[self.index] as! NSManagedObject
            let serverId = (obj.valueForKey("server_id") as! NSNumber).integerValue
            
            //print(ret)
            NSLog("NSMOBJ \(obj)")
            
            print(serverId)
            print(obj.valueForKey("name") as! String)
            
            if(serverId == 0) {
                
                destVC.superIsSynced = false
                
            }else {
                
                destVC.superIsSynced = true
                
            }
        }
        
    }
    
    
    
    
    
}
