//
//  ShoppingListContentViewController.swift
//  Milk
//
//  Created by Jon McLean on 12/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit
import CoreData

class ShoppingListContentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet  var tableView: UITableView!
    
    var items: [AnyObject] = []
    
    var list_id: Int = 0
    
    var superIsSynced: Bool = false
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let createButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: Selector("create"))
        self.navigationItem.rightBarButtonItem = createButton
        
        if(!superIsSynced) {
            createButton.enabled = false
        }else {
            createButton.enabled = true
        }
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let delegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context = delegate.managedObjectContext
        
        let fetch = NSFetchRequest(entityName: "ListContent")
        
        do {
            self.items = try context.executeFetchRequest(fetch)
        }catch {
            print(error)
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ListCell", forIndexPath: indexPath) as! ListCell
        
        let obj = items[indexPath.row]
        
        let name = obj.valueForKey("name") as! String
        
        cell.fillCell(name, index: indexPath.row)
        
        if(!superIsSynced) {
            cell.setDisabled(true)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func create() {
        self.performSegueWithIdentifier("Shopping_CreateLC", sender: self)
    }
    
}
