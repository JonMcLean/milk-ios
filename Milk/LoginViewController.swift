//
//  LoginViewController.swift
//  Milk
//
//  Created by Jon McLean on 9/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginViewController: UIViewController {
    
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var outcomeLabel: UILabel!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.outcomeLabel.text = ""
        self.password.secureTextEntry = true
        
        var tap = UITapGestureRecognizer(target: self, action: Selector("dismissKeyboard"))
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginButton(sender: UIButton) {
        
        print("Login button")
        
        let network = Networking()
        
        network.sendPOSTRequest(globalURL + "/api/auth/login", parameters: ["username":username.text!, "password":password.text!]) { (data, response, error) -> Void in
            
            print("POST Request Sent")
            
            if(error != nil) {
                let handler = ErrorHandler()
                handler.printError(error!, className: "LoginViewController")
            }
            
            let code = (response as! NSHTTPURLResponse).statusCode
            
            if(code == 200) {
                
                print("All is OK")
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    self.outcomeLabel.textColor = UIColor.greenColor()
                    self.outcomeLabel.text = "Success!"
                    
                    let json = JSON(data: data!)
                    let cDic = json.dictionaryObject
                    
                    let storage = SessionStorage()
                    
                    storage.storeSession(cDic!["session"] as! String, expire: cDic!["expire"] as! Int, username: cDic!["username"] as! String, id: cDic!["user_id"] as! Int, firstName: cDic!["first_name"] as! String, lastName: cDic!["last_name"] as! String, icon: cDic!["icon"] as! String)
                    storage.setLoggedIn(true)
                    
                    self.performSegueWithIdentifier("Login_Main", sender: self)
                })
                
            }else if(code == 400) {
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.outcomeLabel.textColor = UIColor.redColor()
                    self.outcomeLabel.text = "Username or Password was incorrect. Try again"
                    
                    self.username.text = ""
                    self.password.text = ""
                })
                
            }else {
                
                print(code)
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.outcomeLabel.textColor = UIColor.redColor()
                    self.outcomeLabel.text = "There was a problem with our servers. Please try again later"
                    
                    self.username.text = ""
                    self.password.text = ""
                })
            }
            
        }
        
    }
    
    @IBAction func moveToSignupButton(sender: UIButton) {
        self.performSegueWithIdentifier("Login_Signup", sender: self)
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
}
