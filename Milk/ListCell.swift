//
//  ListCell.swift
//  Milk
//
//  Created by Jon McLean on 12/04/2016.
//  Copyright © 2016 Jon McLean. All rights reserved.
//

import UIKit
import CoreData

class ListCell: UITableViewCell {
    
    @IBOutlet var cellName: UILabel!
    @IBOutlet var complete: UISwitch!
    var index: Int = 0
    
    func fillCell(name: String, index: Int) {
        self.cellName.text = name
        self.complete.on = false
        self.index = index
    }
    
    @IBAction func switchAction(sender: UISwitch) {
        let isOn: Bool = sender.on
        
        let delegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context = delegate.managedObjectContext
        
        
        let fetch = NSFetchRequest(entityName: "ListContent")
        
        var ret: [AnyObject] = []
        
        do {
            ret = try context.executeFetchRequest(fetch)
        }catch {
            print(error)
        }
        
        let obj = ret[index]
        
        obj.setValue(isOn, forKey: "done")
        
        do {
            try context.save()
        }catch {
            print(error)
        }
        
        print("Overwritten")
    }
    
    func setDisabled(bool: Bool) {
        self.complete.enabled = !bool
    }
    
    
}
